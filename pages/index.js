import Head from 'next/head'
import styles from '../styles/Home.module.css'
import Form from './components/form.js'
import styled from 'styled-components'
import ProductDetails from './components/product_details'
import FreshAirUnitDetail from "./components/fresh_air_unit_detail"
import ExsampleImg from './components/exsample_img'
import ModalShowResponseStatus from './components/modal_show_response_status'
import ShowContacts from './components/show_contacts'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faLine } from '@fortawesome/free-brands-svg-icons'
import React, { useState, useEffect } from 'react';

const Center =  styled.div`
  display: flex;
  justify-content: center;
`
const Line = styled.div`
  width: 240px;
  height: 3px;
  background-color: #3385e7;
  margin: 20px auto;
`
const Content = styled.div`
  max-width: 913px;
  
  div {
    text-indent: 50px;
  }
`
const LeftLineDiv = styled.div`
  text-indent: 0px !important;
  margin-top: 22px;
  border-left: 4px solid  #3385e7;
  padding-left: 15px;
  font-size: 18px;
  line-height: 20px;
  margin-bottom: 40px;
`

const LineButton = styled.button`
  margin-top: 50px;
  color: white;
  background-color: #4ac911;
  border: none;
  padding: 8px 16px;
  border-radius: 50px;
  font-size: 18px;

  display: flex;
  flex-diredtion: row;
  align-items: center;
`

const ButtonLabel = styled.div`
  margin-right: 5px;
  width: fit-content;
  height: 27px;
`
const ButtonIcon = styled.div`
  width: 20px;
  height: 20px;
  display: flex;
  align-items: center;
  justify-contents: center;
`

const LeftContentTitle = styled.div`
  margin-top: 120px;
  text-align: left;
  font-size: 45px;
  font-weight: bold;
`

const LeftContentTitle2 = styled(LeftContentTitle)`
  margin-top: 20px;
`

const LeftContentDetails = styled.div`
  text-align: left;
  line-height: 15px;
`

const SubTitle = styled.div`
  text-align: center;
  font-size: 25px;
  font-weight: bold;
`

const SubTitle2 = styled.div`
  text-align: center;
  font-size: 18px;
  font-weight: bold;
`

const Row = styled.div`
  height: 'fit-content';
  display: flex;
  justify-content: center;
  flex-direction: column;
  margin: 37px 0;
`

export default function Home() {
  const [isOpenModal, setIsOpenModal] = useState(false)
  const [response, setResponse] = useState({})
  const [isReRender, setIsReRender] = useState(false)
  
  useEffect(() => {
    if (isReRender) {
      window.location.reload();
    }
  }, [isReRender])


  return (
    <div className={styles.container}>
      <Head>
        <title>เครื่องกรองอากาศควบคุมเชื้อเติมอากาศสะอาด - tectony</title>
        <link rel="icon" href="/favicon.ico" />
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link href="https://fonts.googleapis.com/css2?family=Kanit:wght@200;300&display=swap" rel="stylesheet" />
      </Head>

      <div className={styles.logo}>
        <img className={styles.imageLogo} 
          src="https://res.cloudinary.com/callmebunbun/f_auto,c_limit,w_256,q_auto/tectony/Logo_air_ql74ln.psd"
        />
      </div>
      <main className={styles.main}>
        <Row>
          <div className={styles.contentTitle}>
            Fresh Air Solution
          </div>
          <SubTitle>
            อากาศสะอาดที่คุณสร้างเองได้
          </SubTitle>
        </Row>
        <Row>
          <div className={styles.contentTitle}>
            HEPA CIRCULATION UNIT: HCU
          </div>
          <SubTitle>
            เครื่องกรองอากาศควบคุมเชื้อ
          </SubTitle>
          <SubTitle2>
            สะอาดปลอดภัย หายใจได้เต็มปอด
          </SubTitle2>
          <Line/>
          <Center>
            <img className={styles.imageContent} 
              src="https://res.cloudinary.com/callmebunbun/f_auto,c_limit,w_3840,q_auto/tectony/fresh%20air/%E0%B8%A3%E0%B8%B9%E0%B8%9B%E0%B8%A0%E0%B8%B2%E0%B8%9E/huc_preview_yuyfzk.png"
            /> 
          </Center>
        </Row>
        <Row>
          <ProductDetails />
        </Row>
        <Row>
          <div className={styles.center}>
            <img className={styles.imageContent} 
              src="https://res.cloudinary.com/callmebunbun/f_auto,c_limit,w_3840,q_auto/tectony/fresh%20air/รูปภาพ%202/hepa_u3z8rg.png"
            />
          </div>
        </Row>
        <Row>
          <LeftContentTitle>
            FRESH AIR UNIT
          </LeftContentTitle>
          <div className={styles.center}>
            <img className={styles.imageContent} 
              src="https://res.cloudinary.com/callmebunbun/f_auto,c_limit,w_3840,q_auto/tectony/fresh%20air/%E0%B8%A3%E0%B8%B9%E0%B8%9B%E0%B8%A0%E0%B8%B2%E0%B8%9E/fresh_air_preview_wva0nk.png"
            />
          </div>
          <FreshAirUnitDetail />
          <div className={styles.center}>
            <img className={styles.imageContent} 
              src="https://res.cloudinary.com/callmebunbun/f_auto,c_limit,w_3840,q_auto/tectony/fresh%20air/%E0%B8%A3%E0%B8%B9%E0%B8%9B%E0%B8%A0%E0%B8%B2%E0%B8%9E/fresh_air_system_ur6e0r.png"
            />
          </div>
        </Row>
        <Row>
          <div className={styles.contentTitle}>
            เติมอากาศสะอาดผ่าน Pre Filter และ HEPA Filter
          </div>
          <LeftContentDetails>
            <p>・ HEPA FILTER สามารถกรองอนุภาคขนาดเล็กได้ถึง 0.3 ไมครอน ที่ประสิทธิภาพ 99.99%</p>
            <p>・ สามารถป้องกัน PM 2.5 และมลพิษจากภายนอกได้\n</p>
            <p>・ สามารถป้องกันการปนเปื้อนของเชื้อไวรัส โควิด-19 จากภายนอกได้\n</p>
          </LeftContentDetails>
        </Row>
        <Row>
          <img className={styles.imageContent} 
            src="https://res.cloudinary.com/callmebunbun/f_auto,c_limit,w_3840,q_auto/tectony/fresh%20air/%E0%B8%A3%E0%B8%B9%E0%B8%9B%E0%B8%A0%E0%B8%B2%E0%B8%9E%202/parimanrom_nzxpsu.png"
          />
          <Center> 4.5 ACH ที่ 16 ตร.ม. - 1 ACH ที่ 64 ตร.ม. ที่ความสูงเพดาน 2.7 ม.</Center>
        </Row>
        <Row>
          <div className={styles.contentTitle}>
            ปกป้องห้องสำคัญด้วย Positive Pressure
          </div>
          <div className={styles.center}>
            <img className={styles.imageContent} 
              src="https://res.cloudinary.com/callmebunbun/f_auto,c_limit,w_3840,q_auto/tectony/fresh%20air/%E0%B8%A3%E0%B8%B9%E0%B8%9B%E0%B8%A0%E0%B8%B2%E0%B8%9E%202/positive_pressure2_oal38x.png"
            />
          </div>
          <Content>
            <p/><b>หลักการ Positive Pressure</b> คือ การทำให้ภายในห้องมีความดันอากาศมากกว่าภายนอกห้อง ความดันในห้องที่มากกว่าจะป้องกันการรั่วไหลของของอากาศภายนอกผ่านรอยรั่วของห้อง เช่น ร่องประตู,ขอบหน้าต่าง,ขอบฝ้าเพดาน และรอยร้าวของผนัง เป็นต้น
          </Content>
        </Row>
        <Row>
          <div className={styles.contentTitle}>
            Smart Infection Control System
          </div>
          <img className={styles.imageContent} 
            src="https://res.cloudinary.com/callmebunbun/f_auto,c_limit,w_3840,q_auto/tectony/fresh%20air/รูปภาพ%202/Image_from_iOS_1_1_1_runihv.gif"
          />
        </Row>
        <Row>
          <LeftContentTitle2>
            HEPA Circulation Unit
          </LeftContentTitle2>
          <Content>
            <div>ระบบสร้างอากาศบริสุทธิ์สำหรับทันตกรรม การออกแบบสอดคล้องกับคำแนะนำตามมาตรฐานกองแบบแผน กระทรวงสาธารณสุข ตามเอกสารเลขที่ ก.45/เม.ย./63 โดยมุ่งเน้นไปที่ส่วนสำคัญ 2 ส่วนหลักคือ</div>
            <div>1. เติมอากาศบริสุทธิ์จากภายนอก ซึ่งผ่านการกรองด้วย HEPA Filter class H13 เข้ามาภายในห้อง 3 ACH</div>
            <div>2. หมุนเวียนอากาศภายในห้องให้บริสุทธิ์ โดยกรองผ่าน HEPA Filter class H14 : EN1822 (2009) เมื่อขณะทำงาน ระบบในห้องสามารถสร้างอากาศสะอาดอยู่ที่ 21 ACH และเมื่อเสร็จงาน การทำงานช่วงพักห้องระบบสามารถสร้างอากาศสะอาดสูงสุดที่ 50 ACH ด้วยการออกแบบระบบนี้ จึงสามารถทำความสะอาดในห้องให้สะอาด 99.9% ภายในระยะเวลา 8 นาที ซึ่งลดระยะเวลาการพักห้อง และสามารถรับเคสต่อไปได้เร็วขึ้น</div>
            <LeftLineDiv>
              8 MINS FOR REMOVAL WITH 99.9% EFFICIENCY
              <p>(50 ACH @ standard Room size 3(w)x4(l)x2.8(h))</p>
            </LeftLineDiv>
          </Content>
        </Row>
        <Row>
          <div className={styles.contentTitle}>
            ตัวอย่างการติดตั้ง
          </div>
        </Row>
        <Row>
          <ExsampleImg 
            src="https://res.cloudinary.com/callmebunbun/f_auto,c_limit,w_3840,q_auto/tectony/fresh%20air/%E0%B8%A3%E0%B8%B9%E0%B8%9B%E0%B8%A0%E0%B8%B2%E0%B8%9E%202/Cafe_lrjf0t.png"
          />
          <ExsampleImg 
            src="https://res.cloudinary.com/callmebunbun/f_auto,c_limit,w_3840,q_auto/tectony/fresh%20air/%E0%B8%A3%E0%B8%B9%E0%B8%9B%E0%B8%A0%E0%B8%B2%E0%B8%9E%202/Classroom_e0dn8p.png"
            label='Cafe'
            labelSide='left'
          />
          <ExsampleImg 
            src="https://res.cloudinary.com/callmebunbun/f_auto,c_limit,w_3840,q_auto/tectony/fresh%20air/%E0%B8%A3%E0%B8%B9%E0%B8%9B%E0%B8%A0%E0%B8%B2%E0%B8%9E%202/Fitness3_gglzv1.png"
            label='Classroom'
            labelSide='right'
          />
          <ExsampleImg 
            src="https://res.cloudinary.com/callmebunbun/f_auto,c_limit,w_3840,q_auto/tectony/fresh%20air/%E0%B8%A3%E0%B8%B9%E0%B8%9B%E0%B8%A0%E0%B8%B2%E0%B8%9E%202/Office_owlopv.png"
            label='Fitness'
            labelSide='left'
          />
          <ExsampleImg 
            src="https://res.cloudinary.com/callmebunbun/f_auto,c_limit,w_3840,q_auto/tectony/fresh%20air/%E0%B8%A3%E0%B8%B9%E0%B8%9B%E0%B8%A0%E0%B8%B2%E0%B8%9E%202/review_product_nhf94f.png"
            label='Office'
            labelSide='right'
          />
           <ExsampleImg 
            src="https://res.cloudinary.com/callmebunbun/f_auto,c_limit,w_3840,q_auto/tectony/fresh%20air/%E0%B8%A3%E0%B8%B9%E0%B8%9B%E0%B8%A0%E0%B8%B2%E0%B8%9E%202/review_300_ycxikr.png"
          />
        </Row>
        <Row>
          <LeftContentTitle2>คลินิกทันตกรรม โรงพยาบาลอู่ทอง</LeftContentTitle2>
          <div className={styles.contentTitle}>
            Fresh Air Solution
          </div>
          <img className={styles.imageContent} 
              src="https://res.cloudinary.com/callmebunbun/f_auto,c_limit,w_3840,q_auto/tectony/fresh%20air/%E0%B8%A3%E0%B8%B9%E0%B8%9B%E0%B8%A0%E0%B8%B2%E0%B8%9E%202/dark_kcrs5n.png"
            />
        </Row>
        <Row>
          <div className={styles.contentTitle}>
            Our Partner
          </div>
          <img className={styles.partner} 
            src="https://res.cloudinary.com/callmebunbun/f_auto,c_limit,w_640,q_auto/tectony/fresh%20air/%E0%B8%A3%E0%B8%B9%E0%B8%9B%E0%B8%A0%E0%B8%B2%E0%B8%9E%202/dark2_xvo1ep.png"
          />
        </Row>
        <Row>
          <div className={styles.contentTitle}>
            Our Clients
          </div>
          <img className={styles.client} 
            src="https://res.cloudinary.com/callmebunbun/f_auto,c_limit,w_3840,q_auto/tectony/fresh%20air/%E0%B8%A3%E0%B8%B9%E0%B8%9B%E0%B8%A0%E0%B8%B2%E0%B8%9E%202/2png_omx0qp.png"
          />
          <Center>
            <LineButton>
              <ButtonLabel>สอบภามเพิ่มเติม LINE</ButtonLabel>
              <ButtonIcon><FontAwesomeIcon icon={faLine}/></ButtonIcon>
            </LineButton>
          </Center>
        </Row>
        <Row>
          <Form 
            setIsOpenModal={setIsOpenModal}
            setResponse={setResponse}
          />
        </Row>
        <Row>
          <ShowContacts />
        </Row>
        {isOpenModal && (
          <ModalShowResponseStatus 
            setIsOpenModal={setIsOpenModal} 
            response={response}
            setIsReRender={setIsReRender}
          />
        )}
      </main>
    </div>
  )
}
