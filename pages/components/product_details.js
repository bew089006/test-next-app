import styled from 'styled-components'

const ProductDetail = styled.div`
  display: flex;
  flex-direction: row;
  margin: 70px 0;
  // justify-content: space-between;
`

const ProductDetailImg = styled.img`
  width: 180px;
  height: 400px;
  margin: 0 50px;
`

const Details = styled.div`
  display: flex;
  flex-direction: column;
  margin-left: 20px;
`

const DetailTitle = styled.div`
  font-size: 2rem;
  font-weight: bold;
`

const DetailRow = styled.div`
  display: flex;
  flex-direction: row;

  margin-top: 10px;
`

const DetailIcon = styled.img`
  width: 42px;
  height: 42px;
  margin: 0 25px;
`

const Line = styled.div`
  margin-top: 5px;
  width: 160px;
  height: 4px;
  background-color: #3385e7;
`
const DetailContent = styled.div`
  width: 500px;
`

const Title = styled.div`
  font-weight: bold;
  font-size: 15px;
`

const Desc = styled.div`
  margin-top: 5px;
  font-size: 13px;
`

function ProductDetails() {
  return (
    <ProductDetail>
      <ProductDetailImg 
        src="https://res.cloudinary.com/callmebunbun/f_auto,c_limit,w_384,q_auto/tectony/fresh%20air/%E0%B8%A3%E0%B8%B9%E0%B8%9B%E0%B8%A0%E0%B8%B2%E0%B8%9E/hcu_profduct_scscko.png"
      /> 
      <Details>
        <DetailTitle>เครื่องกรองอากาศควบคุมเชื้อ</DetailTitle>
        <Line/>
        <DetailRow>
          <DetailIcon src='https://res.cloudinary.com/callmebunbun/f_auto,c_limit,w_96,q_auto/tectony/fresh%20air/icon/%E0%B8%9B%E0%B9%89%E0%B8%AD%E0%B8%87%E0%B8%81%E0%B8%B1%E0%B8%99_COVID-19_%E0%B8%AB%E0%B8%A1%E0%B8%B8%E0%B8%99%E0%B9%80%E0%B8%A7%E0%B8%B5%E0%B8%A2%E0%B8%99%E0%B8%AD%E0%B8%B2%E0%B8%81%E0%B8%B2%E0%B8%A8%E0%B8%AA%E0%B8%B0%E0%B8%AD%E0%B8%B2%E0%B8%94_go7ux7.png'/>
          <DetailContent>
            <Title>หมุนเวียนอากาศสะอาดผ่าน HEPA FILTER</Title>
            <Desc>ประสิทธิภาพการกรอง 99.999% on 0.3 micron</Desc>
          </DetailContent>
        </DetailRow>
        <DetailRow>
          <DetailIcon src='https://res.cloudinary.com/callmebunbun/f_auto,c_limit,w_96,q_auto/tectony/fresh%20air/icon%202/settings_fe7xep.png'/>
          <DetailContent>
            <Title>ติดตั้ง HEPA FILTER Class H14 EN 1822:2009</Title>
            <Desc>ผ่านการทดสอบ Installation Leak Test (ISO14644-3(2005))</Desc>
          </DetailContent>
        </DetailRow>
        <DetailRow>
          <DetailIcon src='https://res.cloudinary.com/callmebunbun/f_auto,c_limit,w_96,q_auto/tectony/fresh%20air/icon/%E0%B8%AD%E0%B8%AD%E0%B8%81%E0%B9%81%E0%B8%9A%E0%B8%9A%E0%B8%AA%E0%B8%AD%E0%B8%94%E0%B8%84%E0%B8%A5%E0%B9%89%E0%B8%AD%E0%B8%87%E0%B8%95%E0%B8%B2%E0%B8%A1%E0%B8%A1%E0%B8%B2%E0%B8%95%E0%B8%A3%E0%B8%90%E0%B8%B2%E0%B8%99_hxbvqe.png'/>
          <DetailContent>
            <Title>ออกแบบสอดคล้องตามมาตรฐาน</Title>
            <Desc>การออกแบบสอดคล้องตามมาตรฐาน กองแบบแผนกระทรวงสาธารณสุข เอกสารเลขที่ ก.45/เม.ย./63</Desc>
          </DetailContent>
        </DetailRow>
        <DetailRow>
          <DetailIcon src='https://res.cloudinary.com/callmebunbun/f_auto,c_limit,w_96,q_auto/tectony/fresh%20air/icon/%E0%B8%9B%E0%B8%A3%E0%B8%B4%E0%B8%A1%E0%B8%B2%E0%B8%93%E0%B8%A5%E0%B8%A1%E0%B8%AA%E0%B8%B9%E0%B8%87_n6sru1.png'/>
          <DetailContent>
            <Title>ปริมาณลมสูง 1,000 CFM</Title>
            <Desc>อัตราหมุนเวียนอากาศ 50 ACH (ห้องขนาดมาตรฐาน 3m x 4m x 2.8m)</Desc>
          </DetailContent>
        </DetailRow>
        <DetailRow>
          <DetailIcon src='https://res.cloudinary.com/callmebunbun/f_auto,c_limit,w_96,q_auto/tectony/fresh%20air/icon/%E0%B9%80%E0%B8%AA%E0%B8%B5%E0%B8%A2%E0%B8%87%E0%B9%80%E0%B8%87%E0%B8%B5%E0%B8%A2%E0%B8%9A_ukargk.png' />
          <DetailContent>
            <Title>เสียงเงียบ</Title>
            <Desc>เสียงเบา ไม่เกิน 10 dB (เทียบกับขณะตอนปิดเครื่อง)</Desc>
          </DetailContent>
        </DetailRow>
        <DetailRow>
          <DetailIcon src='https://res.cloudinary.com/callmebunbun/f_auto,c_limit,w_96,q_auto/tectony/fresh%20air/icon/pm_2.5_plfk8q.png'/>
          <DetailContent>
            <Title>กรองฝุ่น PM 2.5</Title>
            <Desc>เป้องกันวัณโรค แบคทีเรีย เชื้อไวรัส Covid-19</Desc>
          </DetailContent>
        </DetailRow>
      </Details>
  </ProductDetail>
  )
}

export default ProductDetails