import styled from 'styled-components'

const FreshAirUnitDeatils = styled.div`

  margin: 76px 0;
  // margin-left: -150px;
  display: flex;
  flex-direction: column;
`

const Icon = styled.img`
  width: 30px;
  height: 30px;
  margin: 0 20px;
`

const Contents = styled.div`
  display: flex;
  flex-direction: row;
  margin-top: 15px;
`

const Title = styled.div`
  font-size: 2rem;
  font-weight: bold;
`

const Desc = styled.div`
`
const Line = styled.div`
  margin-top: 5px;
  width: 160px;
  height: 4px;
  background-color: #3385e7;
`

function FreshAirUnitDetail() {
  return (
    <FreshAirUnitDeatils>
      <Title>Fresh Air Unit เติมอากาศสะอาดในบ้านเพื่อคนที่คุณรัก</Title>
      <Line />
      <Contents>
        <Icon src='https://res.cloudinary.com/callmebunbun/f_auto,c_limit,w_64,q_auto/tectony/fresh%20air/icon/%E0%B8%AA%E0%B8%A3%E0%B9%89%E0%B8%B2%E0%B8%87%E0%B8%AB%E0%B9%89%E0%B8%AD%E0%B8%87%E0%B8%84%E0%B8%A7%E0%B8%B2%E0%B8%A1%E0%B8%94%E0%B8%B1%E0%B8%99%E0%B8%9A%E0%B8%A7%E0%B8%81_ad8mmt.png'/>
        <Desc>สร้างห้องความดันบวก</Desc>
      </Contents>
      <Contents>
        <Icon src='https://res.cloudinary.com/callmebunbun/f_auto,c_limit,w_64,q_auto/tectony/fresh%20air/icon/%E0%B9%80%E0%B8%95%E0%B8%B4%E0%B8%A1%E0%B8%AD%E0%B8%B2%E0%B8%81%E0%B8%B2%E0%B8%A8%E0%B8%AA%E0%B8%B0%E0%B8%AD%E0%B8%B2%E0%B8%94%E0%B8%9C%E0%B9%88%E0%B8%B2%E0%B8%99_HEPA_FILTER_mpudhq.png'/>
        <Desc>เติมอากาศสะอาดผ่าน HEPA FILTER สามารถกรองอนุภาคขนาดเล็กได้ถึง 0.3 ไมครอน ที่ประสิทธิภาพ 99.99%</Desc>
      </Contents>
      <Contents>
        <Icon src='https://res.cloudinary.com/callmebunbun/f_auto,c_limit,w_64,q_auto/tectony/fresh%20air/icon/%E0%B8%9B%E0%B9%89%E0%B8%AD%E0%B8%87%E0%B8%81%E0%B8%B1%E0%B8%99_PM_2.5_%E0%B9%81%E0%B8%A5%E0%B8%B0%E0%B8%A1%E0%B8%A5%E0%B8%9E%E0%B8%B4%E0%B8%A9%E0%B8%88%E0%B8%B2%E0%B8%81%E0%B8%A0%E0%B8%B2%E0%B8%A2%E0%B8%99%E0%B8%AD%E0%B8%81_dsjogq.png'/>
        <Desc>ป้องกัน PM 2.5 และมลพิษจากภายนอก</Desc>
      </Contents>
      <Contents>
        <Icon src='https://res.cloudinary.com/callmebunbun/f_auto,c_limit,w_64,q_auto/tectony/fresh%20air/icon/%E0%B8%9B%E0%B8%A3%E0%B8%B4%E0%B8%A1%E0%B8%B2%E0%B8%93%E0%B8%A5%E0%B8%A1%E0%B8%AA%E0%B8%B9%E0%B8%87%E0%B8%AA%E0%B8%B8%E0%B8%94_210_CMH_h1sd16.png'/>
        <Desc>ปริมาณลมสูงสุด 210 CMH</Desc>
      </Contents>
    </FreshAirUnitDeatils>
  )
}

export default FreshAirUnitDetail