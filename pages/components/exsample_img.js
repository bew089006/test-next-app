import styled from 'styled-components'

const ExsampleImgContent = styled.div`

`

const ExsampleImgLabel = styled.div`
  margin-top: 34px;
  font-size: 36px;
  text-align: ${(props) => props.side};
`

const ExsampleImgImg = styled.img`
  margin-top: 34px;
  width: 913px;
  height: 514px;
`

const ExsampleImg = (props) => {
  return (
    <ExsampleImgContent>
      <ExsampleImgContent>
        {props.label && (
            <ExsampleImgLabel side={props.labelSide}>{props.label}</ExsampleImgLabel>
        )}
        <ExsampleImgImg 
          src={props.src}
        />
      </ExsampleImgContent>
    </ExsampleImgContent>
  )
}

export default ExsampleImg