import { useState, useEffect } from 'react'
import styled from 'styled-components'
import axios from 'axios'

const ContactDeatils = styled.div`
  display: grid;
  grid-template-columns: auto auto auto;
  padding: 10px;
`
const ContactContent = styled.div`
  margin: 20px 0;
`
const ContactRow = styled.div`
  display: flex;
  flex-direction: row;
`
const ContactLabel = styled.div`
  font-weight: bold;
  margin-right: 10px; 
  width: 100px;
`
const ContactValue = styled.div`
`

const Index = styled.div`
  font-size: 24px;
  font-weight: bold;
  text-align: center;
`

const ShowContacts = (props) => {
  const [contacts, getContacts] = useState([]);

  useEffect(() => {
    getAllContacts()
  }, [])

  const getAllContacts = () => {
    const api_host = process.env.apiHost 
  
    axios({
      methos: "GET",
      url: api_host + "/contacts"
    }).then((res)=> {
        const allContacts = res.data.contacts
        getContacts(allContacts)
    }).catch(error => console.error(`Error: ${error}` ))
    
  }

  return (
    <ContactDeatils>
      {contacts.map((contact, index) => (
        <ContactContent>
          <Index>number {index + 1}</Index>
          <ContactRow>
            <ContactLabel>First Name</ContactLabel>
            <ContactValue>{contact.first_name}</ContactValue>
          </ContactRow>
          <ContactRow>
            <ContactLabel>Last Name</ContactLabel>
            <ContactValue>{contact.last_name}</ContactValue>
          </ContactRow>
          <ContactRow>
            <ContactLabel>Telephone</ContactLabel>
            <ContactValue>{contact.telephone}</ContactValue>
          </ContactRow>
          <ContactRow>
            <ContactLabel>Email</ContactLabel>
            <ContactValue>{contact.email}</ContactValue>
          </ContactRow>
        </ContactContent>
      ))}
    </ContactDeatils>
  )
}

export default ShowContacts