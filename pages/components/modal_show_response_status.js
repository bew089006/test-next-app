import styled from 'styled-components'

const Modal = styled.div`
  position: fixed; 
  z-index: 1;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%; 
  backdrop-filter: blur(5px);
`
const Container = styled.div`
  width: 500px;
  height: 500px;
  background-color: #e6e3e3;
  margin: auto auto;
  margin-top: 100px;
  border-radius: 50px;
  z-index: 2;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`
const Title = styled.div``
const Desc = styled.div``

const Button = styled.button`
  width: 100px;
  height: 35px;
  border-radius: 50px;
  border: none;
  color: white;
  background-color: ${(props) => props.color};
`

const ModalShowResponseStatus = props => {
  return (
    <Modal>
      <Container>
        {console.log(props.response)}
        <Title>{props.response.status}</Title>
        <Desc>{props.response.desc}</Desc>
        <Button 
          color={props.response.status === 'success' ? "green" : "red"}
          onClick={()=> {
            props.setIsOpenModal(false)
            props.setIsReRender(props.response.status === 'success' ? true : false)
          }}
        >
          {props.response.status === 'success' ? "Success" : "Go Back."}
        </Button>
      </Container>
    </Modal>
  )
}

export default ModalShowResponseStatus