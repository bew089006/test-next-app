// import styles from '../../styles/form.css'
import styled from 'styled-components'
import axios from 'axios'
import {useState} from 'react'
import { Formik } from 'formik';

const FormContainer = styled.div`
  margin-top: 20px;
`

const FormTitle = styled.div`
  font-size: 45px;
  font-weight: bold;
  text-align: center;
`

const FormControl = styled.div`
  flex-direction: row;
  justify-content: center;
`

const FormDiv = styled.div`
  margin-top: 20px;
  display: flex;
  flex-direction: row;
`

const FieldLabel = styled.div`
  width: 50%;
  font-size: 20px;
  font-weight: bold;
`

const FieldInput = styled.input`
  width: 50%; 
`
const ButtonDiv = styled.div`
  text-align: center;
`
const SubmitButton = styled.button`
  margin-top: 20px;
  margin-left: auto;
  margin-right: auto;

  text-align: center;
  width: 100px;
  height: 30px;
  background-color: green;
  border: none;
  border-radius: 50px;
  font-size: 18px;
  color: white;
`
const ValidateMessage = styled.div`
  width: 20px;
  margin-left: 10px;
  color: red;
`

const FormField = props => {
  function checkValidate() {
    if (props.values[props.fieldName]) {
      return false
    } else {
      return true
    }
  }

  return (
    <FormDiv>
      <FieldLabel>{props.labelName}</FieldLabel>
      <FieldInput
        type='text'
        name={props.fieldName}
        value={props.values.name}
        onChange={(e) => {
          props.setFieldValue(props.fieldName, e.target.value)
        }}
      />
      <ValidateMessage>
        {(props.isSubmitted && checkValidate())&& 'required'}
      </ValidateMessage>
    </FormDiv>
  )
}

const Form = props => {
  const [isSubmitted, setIsSubmitted] = useState(false)

  const createContactUs = async(event, params) => {
    const api_host = process.env.apiHost 

    console.log("check api host", process.env)
    axios({
      method: "POST",
      url: api_host + "/contacts",
      headers: {
        "Content-Type": "application/json"
      },
      data: params,
    }).then((response) => {
      props.setIsOpenModal(true)
      props.setResponse(response.data)
    }).catch((error) => {
      props.setIsOpenModal(true)
      props.setResponse(error.response.data)
    })
  }

  return (
    <FormContainer>
      <FormTitle>Contact us</FormTitle>
      <FormControl>
        <Formik
          initialValues={
            { 
              first_name: '',
              last_name: '',
              telephone: '',
              email: '' 
            }
          }
          onSubmit={(values, action) => {
            setIsSubmitted(true)
            createContactUs(action, values)
          }}
        >
          {props => (
            <form onSubmit={props.handleSubmit}>
              <FormField fieldName='first_name' labelName='First Name' {...props} isSubmitted={isSubmitted} />
              <FormField fieldName='last_name' labelName='Last Name' {...props} isSubmitted={isSubmitted}/>
              <FormField fieldName='telephone' labelName='Telephone' {...props} isSubmitted={isSubmitted}/>
              <FormField fieldName='email' labelName='Email' {...props} isSubmitted={isSubmitted}/>
              <ButtonDiv>
                <SubmitButton type='submit'>Submit</SubmitButton>
              </ButtonDiv>
            </form>
          )}
        </Formik>
      </FormControl>
    </FormContainer>
  )
}

export default Form